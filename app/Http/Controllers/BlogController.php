<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public function index(){
        $blogs = Cache::remember('blogs', 2, function() {
            return Blog::all();
        });
        return view('blogs', compact('blogs'));
    }

    public function single($title){
        $blog = Cache::remember('blog-'.$title, 2, function() use($title) {
            return Blog::where('title', $title)->first();
        });
        return view('single', compact('blog'));
    }
}
